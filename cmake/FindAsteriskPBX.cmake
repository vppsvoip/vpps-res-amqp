# Locate Asterisk PBX includes and library
# This module defines
# ASTERISK_LIBRARY_DIR, the name of the asterisk modules library
# ASTERISK_INCLUDE_DIR, where to find asterisk includes

set(ASTERISK_FOUND "NO")

find_path(ASTERISK_INCLUDE_DIR asterisk.h
    PATHS
        /usr/include
)

find_path(ASTERISK_LIBRARY_DIR pbx_config.so
    PATH_SUFFIXES modules
    PATHS
        /usr/lib/asterisk
        /usr/lib64/asterisk
)

if(ASTERISK_LIBRARY_DIR)
    set(ASTERISK_FOUND "YES")
endif()

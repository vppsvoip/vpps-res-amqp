﻿# vpps-res-amqp

Asterisk IPBX module to connect and publish to an AMQP server.  

To install

```shell
yum install gcc make cmake3 libtool rabbitmq-devel asterisk-devel git
git clone https://gitlab.com/vppsvoip/vpps-res-amqp.git
cd vpps-res-amqp
mkdir build
pushd build
cmake -DCMAKE_INSTALL_PREFIX=/ -DCMAKE_BUILD_TYPE=Release ..
make
sudo make install
popd
```

Configure the file in `/etc/asterisk/amqp.conf`. Please restart asterisk before loading `res_amqp.so` for the documentation.  

To load module:

```
CLI> module load res_amqp.so
```

There is a `amqp` command on the CLI to get the status.
